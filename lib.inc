section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

exit:
    mov rax, 60
    syscall
    ret

string_length:
    xor rax, rax
    .strlen_loop:
        cmp byte [rdi + rax], 0x0
        je .strlen_finish
        inc rax
        jmp .strlen_loop
    .strlen_finish:
        ret

print_string:
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    pop rdi
    ret

print_newline:
    mov rdi, 0xA
    jmp print_char

print_uint:
    mov rax, rdi
    mov rcx, 10
    mov rbp, rsp
    push 0x0
    .print_uint_loop:
        xor rdx, rdx
        div rcx
        add rdx, '0'
        dec rsp
        mov [rsp], dl
        cmp rax, 0
        jne .print_uint_loop
    mov rdi, rsp
    call print_string
    mov rsp, rbp
    ret

print_int:
    cmp rdi, 0
    jnl .print_positive
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    .print_positive:
        jmp print_uint

string_equals:
    call string_length
    mov rcx, rax
    xchg rdi, rsi
    call string_length
    mov r8, rax
    mov rax, 0
    cmp r8, rcx
    jne .equals_finish
    mov rcx, 0
    .equals_loop:
        mov byte dl, [rsi + rcx]
        mov byte dh, [rdi + rcx]
        cmp dl, dh
        jne .equals_finish
        inc rcx
        cmp rcx, r8
        jl .equals_loop
    mov rax, 1
    .equals_finish:
        ret

read_char:
    dec rsp
    mov byte [rsp], 0x0
    mov rsi, rsp
    mov rdx, 1
    mov rax, 0
    mov rdi, 0
    syscall
    xor rax, rax
    mov al, [rsi]
    inc rsp
    ret

read_word:
    mov r8, rdi                 ; buffer address
    mov r9, rsi                 ; buffer size

    cmp r9, 0
    jng .small_buffer

    .check_start_space:
        call read_char
        cmp al, 0x0
        je .input_ended_early
        cmp al, 0x9
        je .check_start_space
        cmp al, 0xA
        je .check_start_space
        cmp al, 0x20
        je .check_start_space 

    mov r10, 1                  ; found the word beginning, setting length to 1
    mov [r8], al                ; saving the first char

    .continue_reading:
        cmp r10, r9
        jnl .small_buffer
        
        call read_char
        cmp al, 0x0
        je .word_ended
        cmp al, 0x9
        je .word_ended
        cmp al, 0xA
        je .word_ended
        cmp al, 0x20
        je .word_ended

        mov [r8 + r10], al
        inc r10
        jmp .continue_reading

    .word_ended:
        mov rax, r8
        mov rdx, r10
        mov byte [r8 + r10], 0x0
        jmp .finish_reading

    .input_ended_early:
        mov rax, r8
        mov rdx, 0
        jmp .finish_reading

    .small_buffer:
        mov rax, 0

    .finish_reading:
        ret

parse_uint:
    mov rax, 0
    mov rcx, 0
    mov r9, 10
    
    .parse_uint_loop:
        mov r8, 0
        mov r8b, byte [rdi + rcx]

        cmp r8b, '0'
        jl .parse_uint_finish

        cmp r8b, '9'
        jg .parse_uint_finish

        sub r8b, '0'
        mul r9
        add rax, r8

        inc rcx
        jmp .parse_uint_loop

    .parse_uint_finish:
        mov rdx, rcx
        ret

parse_int:
    cmp byte [rdi], '-'
    jne .parse_positive

    inc rdi
    call parse_uint
    cmp rdx, 0
    je .parse_int_finish
    neg rax
    inc rdx

    .parse_int_finish:
        ret

    .parse_positive:
        jmp parse_uint

string_copy:
    call string_length
    inc rax

    cmp rax, rdx
    jg .strcpy_fail

    mov rcx, 0

    .strcpy_loop:
        cmp byte [rdi + rcx], 0x0
        je .strcpy_terminate
        mov r8b, [rdi + rcx]
        mov [rsi + rcx], r8b
        inc rcx
        jmp .strcpy_loop

    .strcpy_terminate:
        mov byte [rsi + rcx], 0x0
        mov rax, rcx
        jmp .strcpy_finish

    .strcpy_fail:
        mov rax, 0

    .strcpy_finish:
        ret
